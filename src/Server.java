import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by omfg on 30.11.16.
 */
public class Server implements Runnable {
    private static ServerSocket server;
    public static  Socket connection;
    static private ObjectOutputStream output;
    static private ObjectInputStream input;


    @Override
    public void run() {
        try{
            server=new ServerSocket(5678,10);
            while (true){
                connection = server.accept();
                output = new ObjectOutputStream(connection.getOutputStream());
                input = new ObjectInputStream(connection.getInputStream());
                output.writeObject("You sent: "+(String)input.readObject());
            }
        }catch (UnknownHostException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (HeadlessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
